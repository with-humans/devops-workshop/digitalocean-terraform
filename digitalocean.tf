resource "digitalocean_kubernetes_cluster" "primary" {
  name    = var.doks_cluster_name
  region  = var.doks_cluster_region
  version = var.doks_cluster_version

  node_pool {
    name       = "${var.doks_cluster_name}-pool"
    size       = var.doks_cluster_pool_size
    node_count = var.doks_cluster_pool_node_count
  }
}

resource "digitalocean_domain" "default" {
  name = "k8s.with-humans.org"
}

resource "digitalocean_project" "cicd-workshop" {
  name        = "CI/CD Workshop"
  description = "project description"
  purpose     = "Web Application"
  environment = "Production"
  resources = [
    digitalocean_kubernetes_cluster.primary.urn,
    digitalocean_domain.default.urn
  ]
}

resource "digitalocean_record" "www" {
  domain = digitalocean_domain.default.id
  type   = "A"
  name   = "@"
  value  = local.load_balancer_ip
}

resource "digitalocean_record" "any" {
  domain = digitalocean_domain.default.id
  type   = "A"
  name   = "*"
  value  = local.load_balancer_ip
}

