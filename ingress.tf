data "kubernetes_service" "traefik-loadbalancer" {
  depends_on = [
    kubectl_manifest.sync
  ]
  metadata {
    name      = "traefik"
    namespace = "traefik"
  }
}

locals {
  load_balancer_ip = data.kubernetes_service.traefik-loadbalancer.status.0.load_balancer.0.ingress.0.ip
}

